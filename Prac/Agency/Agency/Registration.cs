﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Agency
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        dataBase db = new dataBase();
        public string clientID;
        DataSet dataset = new DataSet();
        private void Registration_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
         
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (textBox5.UseSystemPasswordChar == true)
            {
                textBox5.UseSystemPasswordChar = false;
            }
            else
            {
                textBox5.UseSystemPasswordChar = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "" && maskedTextBox1.Text != "")
            {
                db.insertInfo("Клиенты", $"'{textBox1.Text}','{textBox2.Text}','{textBox3.Text}','{maskedTextBox1.Text}','{textBox4.Text + comboBox1.SelectedItem.ToString()}', +'{textBox5.Text}'");
                MessageBox.Show("Вы успешно зарегисрировались", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                Auth auth = new Auth();
                auth.Show();
            }
            else
            {
                MessageBox.Show("Заполните все необходимые поля", "Регистрация", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Registration_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }
    }
}
