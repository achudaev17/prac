﻿namespace Agency
{
    partial class Selection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.quit = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.profileLink = new System.Windows.Forms.Label();
            this.dealLink = new System.Windows.Forms.Label();
            this.offerLink = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.apply = new System.Windows.Forms.Button();
            this.maxFloors = new System.Windows.Forms.MaskedTextBox();
            this.minFloors = new System.Windows.Forms.MaskedTextBox();
            this.lMaxFloors = new System.Windows.Forms.Label();
            this.lMinFloors = new System.Windows.Forms.Label();
            this.maxFloor = new System.Windows.Forms.MaskedTextBox();
            this.minFloor = new System.Windows.Forms.MaskedTextBox();
            this.lMaxFloor = new System.Windows.Forms.Label();
            this.lMinFloor = new System.Windows.Forms.Label();
            this.maxRooms = new System.Windows.Forms.MaskedTextBox();
            this.minRooms = new System.Windows.Forms.MaskedTextBox();
            this.maxSquare = new System.Windows.Forms.MaskedTextBox();
            this.minSquare = new System.Windows.Forms.MaskedTextBox();
            this.maxPrice = new System.Windows.Forms.MaskedTextBox();
            this.minPrice = new System.Windows.Forms.MaskedTextBox();
            this.lMaxRooms = new System.Windows.Forms.Label();
            this.lMinRooms = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.district = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.type = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(209)))));
            this.panel1.Controls.Add(this.quit);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.profileLink);
            this.panel1.Controls.Add(this.dealLink);
            this.panel1.Controls.Add(this.offerLink);
            this.panel1.Location = new System.Drawing.Point(1, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 541);
            this.panel1.TabIndex = 3;
            // 
            // quit
            // 
            this.quit.AutoSize = true;
            this.quit.BackColor = System.Drawing.Color.Transparent;
            this.quit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.quit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.quit.Location = new System.Drawing.Point(22, 295);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(63, 20);
            this.quit.TabIndex = 4;
            this.quit.Text = "Выход";
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Agency.Properties.Resources.logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 121);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // profileLink
            // 
            this.profileLink.AutoSize = true;
            this.profileLink.BackColor = System.Drawing.Color.Transparent;
            this.profileLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.profileLink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.profileLink.Location = new System.Drawing.Point(22, 146);
            this.profileLink.Name = "profileLink";
            this.profileLink.Size = new System.Drawing.Size(89, 20);
            this.profileLink.TabIndex = 2;
            this.profileLink.Text = "Профиль";
            this.profileLink.Click += new System.EventHandler(this.profileLink_Click);
            // 
            // dealLink
            // 
            this.dealLink.AutoSize = true;
            this.dealLink.BackColor = System.Drawing.Color.Transparent;
            this.dealLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dealLink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.dealLink.Location = new System.Drawing.Point(22, 246);
            this.dealLink.Name = "dealLink";
            this.dealLink.Size = new System.Drawing.Size(73, 20);
            this.dealLink.TabIndex = 1;
            this.dealLink.Text = "Сделка";
            this.dealLink.Click += new System.EventHandler(this.dealLink_Click);
            // 
            // offerLink
            // 
            this.offerLink.AutoSize = true;
            this.offerLink.BackColor = System.Drawing.Color.Transparent;
            this.offerLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.offerLink.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.offerLink.Location = new System.Drawing.Point(22, 196);
            this.offerLink.Name = "offerLink";
            this.offerLink.Size = new System.Drawing.Size(204, 20);
            this.offerLink.TabIndex = 0;
            this.offerLink.Text = "Подбор недвижимости";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.apply);
            this.groupBox1.Controls.Add(this.maxFloors);
            this.groupBox1.Controls.Add(this.minFloors);
            this.groupBox1.Controls.Add(this.lMaxFloors);
            this.groupBox1.Controls.Add(this.lMinFloors);
            this.groupBox1.Controls.Add(this.maxFloor);
            this.groupBox1.Controls.Add(this.minFloor);
            this.groupBox1.Controls.Add(this.lMaxFloor);
            this.groupBox1.Controls.Add(this.lMinFloor);
            this.groupBox1.Controls.Add(this.maxRooms);
            this.groupBox1.Controls.Add(this.minRooms);
            this.groupBox1.Controls.Add(this.maxSquare);
            this.groupBox1.Controls.Add(this.minSquare);
            this.groupBox1.Controls.Add(this.maxPrice);
            this.groupBox1.Controls.Add(this.minPrice);
            this.groupBox1.Controls.Add(this.lMaxRooms);
            this.groupBox1.Controls.Add(this.lMinRooms);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.district);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.type);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(238, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(798, 202);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Фильтры";
            // 
            // apply
            // 
            this.apply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(209)))));
            this.apply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apply.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.apply.Location = new System.Drawing.Point(644, 159);
            this.apply.Name = "apply";
            this.apply.Size = new System.Drawing.Size(140, 37);
            this.apply.TabIndex = 30;
            this.apply.Text = "Применить";
            this.apply.UseVisualStyleBackColor = false;
            this.apply.Click += new System.EventHandler(this.apply_Click);
            // 
            // maxFloors
            // 
            this.maxFloors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxFloors.Location = new System.Drawing.Point(613, 112);
            this.maxFloors.Mask = "99";
            this.maxFloors.Name = "maxFloors";
            this.maxFloors.Size = new System.Drawing.Size(140, 22);
            this.maxFloors.TabIndex = 29;
            // 
            // minFloors
            // 
            this.minFloors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minFloors.Location = new System.Drawing.Point(463, 112);
            this.minFloors.Mask = "99";
            this.minFloors.Name = "minFloors";
            this.minFloors.Size = new System.Drawing.Size(132, 22);
            this.minFloors.TabIndex = 28;
            // 
            // lMaxFloors
            // 
            this.lMaxFloors.AutoSize = true;
            this.lMaxFloors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lMaxFloors.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lMaxFloors.Location = new System.Drawing.Point(610, 93);
            this.lMaxFloors.Name = "lMaxFloors";
            this.lMaxFloors.Size = new System.Drawing.Size(119, 16);
            this.lMaxFloors.TabIndex = 27;
            this.lMaxFloors.Text = "Макс. этажность:";
            // 
            // lMinFloors
            // 
            this.lMinFloors.AutoSize = true;
            this.lMinFloors.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lMinFloors.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lMinFloors.Location = new System.Drawing.Point(460, 93);
            this.lMinFloors.Name = "lMinFloors";
            this.lMinFloors.Size = new System.Drawing.Size(113, 16);
            this.lMinFloors.TabIndex = 26;
            this.lMinFloors.Text = "Мин. этажность:";
            // 
            // maxFloor
            // 
            this.maxFloor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxFloor.Location = new System.Drawing.Point(309, 159);
            this.maxFloor.Mask = "99";
            this.maxFloor.Name = "maxFloor";
            this.maxFloor.Size = new System.Drawing.Size(140, 22);
            this.maxFloor.TabIndex = 25;
            // 
            // minFloor
            // 
            this.minFloor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minFloor.Location = new System.Drawing.Point(159, 159);
            this.minFloor.Mask = "99";
            this.minFloor.Name = "minFloor";
            this.minFloor.Size = new System.Drawing.Size(132, 22);
            this.minFloor.TabIndex = 24;
            // 
            // lMaxFloor
            // 
            this.lMaxFloor.AutoSize = true;
            this.lMaxFloor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lMaxFloor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lMaxFloor.Location = new System.Drawing.Point(306, 140);
            this.lMaxFloor.Name = "lMaxFloor";
            this.lMaxFloor.Size = new System.Drawing.Size(82, 16);
            this.lMaxFloor.TabIndex = 23;
            this.lMaxFloor.Text = "Макс. этаж:";
            // 
            // lMinFloor
            // 
            this.lMinFloor.AutoSize = true;
            this.lMinFloor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lMinFloor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lMinFloor.Location = new System.Drawing.Point(156, 140);
            this.lMinFloor.Name = "lMinFloor";
            this.lMinFloor.Size = new System.Drawing.Size(76, 16);
            this.lMinFloor.TabIndex = 22;
            this.lMinFloor.Text = "Мин. этаж:";
            // 
            // maxRooms
            // 
            this.maxRooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxRooms.Location = new System.Drawing.Point(309, 110);
            this.maxRooms.Mask = "99";
            this.maxRooms.Name = "maxRooms";
            this.maxRooms.Size = new System.Drawing.Size(140, 22);
            this.maxRooms.TabIndex = 21;
            // 
            // minRooms
            // 
            this.minRooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minRooms.Location = new System.Drawing.Point(159, 110);
            this.minRooms.Mask = "99";
            this.minRooms.Name = "minRooms";
            this.minRooms.Size = new System.Drawing.Size(132, 22);
            this.minRooms.TabIndex = 20;
            // 
            // maxSquare
            // 
            this.maxSquare.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxSquare.Location = new System.Drawing.Point(613, 50);
            this.maxSquare.Mask = "99999999999999999999999";
            this.maxSquare.Name = "maxSquare";
            this.maxSquare.Size = new System.Drawing.Size(157, 22);
            this.maxSquare.TabIndex = 19;
            // 
            // minSquare
            // 
            this.minSquare.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minSquare.Location = new System.Drawing.Point(450, 50);
            this.minSquare.Mask = "99999999999999999999999";
            this.minSquare.Name = "minSquare";
            this.minSquare.Size = new System.Drawing.Size(157, 22);
            this.minSquare.TabIndex = 18;
            // 
            // maxPrice
            // 
            this.maxPrice.BackColor = System.Drawing.SystemColors.Window;
            this.maxPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxPrice.ForeColor = System.Drawing.Color.Black;
            this.maxPrice.Location = new System.Drawing.Point(302, 50);
            this.maxPrice.Mask = "99999999999999999999999";
            this.maxPrice.Name = "maxPrice";
            this.maxPrice.Size = new System.Drawing.Size(133, 22);
            this.maxPrice.TabIndex = 17;
            // 
            // minPrice
            // 
            this.minPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minPrice.Location = new System.Drawing.Point(159, 50);
            this.minPrice.Mask = "99999999999999999999999";
            this.minPrice.Name = "minPrice";
            this.minPrice.Size = new System.Drawing.Size(133, 22);
            this.minPrice.TabIndex = 16;
            // 
            // lMaxRooms
            // 
            this.lMaxRooms.AutoSize = true;
            this.lMaxRooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lMaxRooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lMaxRooms.Location = new System.Drawing.Point(306, 91);
            this.lMaxRooms.Name = "lMaxRooms";
            this.lMaxRooms.Size = new System.Drawing.Size(143, 16);
            this.lMaxRooms.TabIndex = 15;
            this.lMaxRooms.Text = "Макс. кол-во комнат:";
            // 
            // lMinRooms
            // 
            this.lMinRooms.AutoSize = true;
            this.lMinRooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lMinRooms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.lMinRooms.Location = new System.Drawing.Point(156, 91);
            this.lMinRooms.Name = "lMinRooms";
            this.lMinRooms.Size = new System.Drawing.Size(137, 16);
            this.lMinRooms.TabIndex = 13;
            this.lMinRooms.Text = "Мин. кол-во комнат:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label5.Location = new System.Drawing.Point(610, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(166, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Максимальная площадь:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label6.Location = new System.Drawing.Point(447, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Минимальная площадь:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label4.Location = new System.Drawing.Point(299, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Максимальная цена:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label3.Location = new System.Drawing.Point(156, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Минимальная цена:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label2.Location = new System.Drawing.Point(6, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Желаемый район:";
            // 
            // district
            // 
            this.district.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.district.FormattingEnabled = true;
            this.district.Items.AddRange(new object[] {
            "Все районы"});
            this.district.Location = new System.Drawing.Point(9, 110);
            this.district.Name = "district";
            this.district.Size = new System.Drawing.Size(129, 24);
            this.district.TabIndex = 2;
            this.district.SelectedIndexChanged += new System.EventHandler(this.district_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Тип недвижимости:";
            // 
            // type
            // 
            this.type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.type.FormattingEnabled = true;
            this.type.Items.AddRange(new object[] {
            "Выберите тип",
            "Квартира",
            "Дом",
            "Земля"});
            this.type.Location = new System.Drawing.Point(9, 50);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(129, 24);
            this.type.TabIndex = 0;
            this.type.SelectedIndexChanged += new System.EventHandler(this.type_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(238, 209);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(798, 191);
            this.dataGridView1.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label7.Location = new System.Drawing.Point(235, 412);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(568, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Заполните форму ниже, если вы нашли подходящую для вас недвижимость.";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.maskedTextBox1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.groupBox2.Location = new System.Drawing.Point(238, 431);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(400, 82);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Отправить запрос на покупку недвижимости";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(209)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(301, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 37);
            this.button1.TabIndex = 31;
            this.button1.Text = "Отправить";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBox1.Location = new System.Drawing.Point(159, 21);
            this.maskedTextBox1.Mask = "99999999999999999999";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(132, 22);
            this.maskedTextBox1.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            this.label8.Location = new System.Drawing.Point(6, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 16);
            this.label8.TabIndex = 31;
            this.label8.Text = "Номер недвижимости:";
            // 
            // Selection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.ClientSize = new System.Drawing.Size(1040, 525);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Selection";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Selection_FormClosing);
            this.Load += new System.EventHandler(this.Selection_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label quit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label profileLink;
        private System.Windows.Forms.Label dealLink;
        private System.Windows.Forms.Label offerLink;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox district;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox type;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox maxRooms;
        private System.Windows.Forms.MaskedTextBox minRooms;
        private System.Windows.Forms.MaskedTextBox maxSquare;
        private System.Windows.Forms.MaskedTextBox minSquare;
        private System.Windows.Forms.MaskedTextBox maxPrice;
        private System.Windows.Forms.MaskedTextBox minPrice;
        private System.Windows.Forms.Label lMaxRooms;
        private System.Windows.Forms.Label lMinRooms;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox maxFloors;
        private System.Windows.Forms.MaskedTextBox minFloors;
        private System.Windows.Forms.Label lMaxFloors;
        private System.Windows.Forms.Label lMinFloors;
        private System.Windows.Forms.MaskedTextBox maxFloor;
        private System.Windows.Forms.MaskedTextBox minFloor;
        private System.Windows.Forms.Label lMaxFloor;
        private System.Windows.Forms.Label lMinFloor;
        private System.Windows.Forms.Button apply;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label8;
    }
}