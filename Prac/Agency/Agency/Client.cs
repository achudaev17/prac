﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Agency
{
    public partial class Client : Form
    {
        public Client()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        dataBase db = new dataBase();
        public string clientID;
        DataSet dataset = new DataSet();
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {

                string query = $"update Клиенты set Фамилия = '{textBox1.Text}',Имя = '{textBox2.Text}'," +
                    $"Отчество = '{textBox3.Text}',Телефон = '{textBox4.Text}',Электронная_почта = '{textBox5.Text}'," +
                    $"Пароль = '{textBox6.Text}' where ID_Клиента = {clientID}";
                adapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                adapter.Fill(dataset);
            }
            MessageBox.Show("Данные успешно изменены.", "Изменение данных", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        }

        private void Client_Load(object sender, EventArgs e)
        {

            using (SqlConnection connection = new SqlConnection(db.connect()))
            {

                string query = $"select Фамилия,Имя,Отчество,Телефон,Электронная_почта,Пароль from Клиенты where ID_Клиента = {clientID}";
                adapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count;i++)
                {
                  
                   textBox1.Text =  dataset.Tables[0].Rows[i][0].ToString();
                   textBox2.Text = dataset.Tables[0].Rows[i][1].ToString();
                   textBox3.Text = dataset.Tables[0].Rows[i][2].ToString();
                   textBox4.Text = dataset.Tables[0].Rows[i][3].ToString();
                   textBox5.Text = dataset.Tables[0].Rows[i][4].ToString();
                   textBox6.Text = dataset.Tables[0].Rows[i][5].ToString();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dialog;
            dialog = MessageBox.Show("Вы уверены,что хотите удалить свой профиль?", "Удаление профиля", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            if (dialog == DialogResult.OK)
            {
                using (SqlConnection connection = new SqlConnection(db.connect()))
                {
                    string query = $"delete from Клиенты where ID_Клиента = {clientID}";
                    adapter = new SqlDataAdapter(query, connection);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    MessageBox.Show("Ваша учетная запись успешно удалена!", "Удаление учетной записи", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Hide();
                    Auth auth = new Auth();
                    auth.Show();
                }
            }

        }

        private void offerLink_Click(object sender, EventArgs e)
        {
            Selection selection = new Selection();
            selection.clientID = clientID;
            this.Hide();
            selection.Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }

        private void dealLink_Click(object sender, EventArgs e)
        {
            Deal deal = new Deal();
            deal.clientID = clientID;
            this.Hide();
            deal.Show();
        }

        private void Profile_Enter(object sender, EventArgs e)
        {

        }

        private void Client_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
