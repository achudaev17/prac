﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Agency
{
    public partial class DealsRealtor : Form
    {
        public DealsRealtor()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        dataBase db = new dataBase();
        public string realtorID;
        DataSet dataset = new DataSet();

        private void DealsRealtor_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.getInfo("Заявки_на_покупку_недвижимости", " where ID_Риэлтора is null");
            dataGridView2.DataSource = db.getInfo("Объекты_недвижимости", " where Риэлтор is null");
        }

        private void profileLink_Click(object sender, EventArgs e)
        {
            Realtor realtor = new Realtor();
            realtor.realtorID = realtorID;
            this.Hide();
            realtor.Show();
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            db.updateInfo("Заявки_на_покупку_недвижимости", $"ID_Риэлтора = {realtorID} where ID_Заявки = {maskedTextBox1.Text}");
            db.updateInfo("Заявки_на_покупку_недвижимости", $"Статус = 1 where ID_Заявки = {maskedTextBox1.Text}");
            dataGridView1.DataSource = db.getInfo("Заявки_на_покупку_недвижимости", " where ID_Риэлтора is null");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            db.updateInfo("Объекты_недвижимости", $"Риэлтор = {realtorID} where ID_Недвижимости = {maskedTextBox2.Text}");
            db.updateInfo("Объекты_недвижимости", $"Статус = 1 where ID_Недвижимости = {maskedTextBox2.Text}");
            dataGridView2.DataSource = db.getInfo("Объекты_недвижимости", " where Риэлтор is null");
        }

        private void DealsRealtor_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
