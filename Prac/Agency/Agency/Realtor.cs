﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Agency
{
    public partial class Realtor : Form
    {
        public Realtor()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        dataBase db = new dataBase();
        public string realtorID;
        DataSet dataset = new DataSet();
        private void Realtor_Load(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {

                string query = $"select Фамилия,Имя,Отчество,Пароль from Риэлторы where ID_Риэлтора = {realtorID}";
                adapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                adapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    textBox1.Text = dataset.Tables[0].Rows[i][0].ToString();
                    textBox2.Text = dataset.Tables[0].Rows[i][1].ToString();
                    textBox3.Text = dataset.Tables[0].Rows[i][2].ToString();
                    textBox4.Text = dataset.Tables[0].Rows[i][3].ToString();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dialog;
            dialog = MessageBox.Show("Вы уверены,что хотите удалить свой профиль?", "Удаление профиля", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            if (dialog == DialogResult.OK)
            {
                using (SqlConnection connection = new SqlConnection(db.connect()))
                {
                    string query = $"delete from Риэлторы where ID_Риэлтора = {realtorID}";
                    adapter = new SqlDataAdapter(query, connection);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    MessageBox.Show("Ваша учетная запись успешно удалена!", "Удаление учетной записи", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Hide();
                    Auth auth = new Auth();
                    auth.Show();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {

                string query = $"update Риэлторы set Фамилия = '{textBox1.Text}',Имя = '{textBox2.Text}'," +
                    $"Отчество = '{textBox3.Text}'," +
                    $"Пароль = '{textBox4.Text}' where ID_Клиента = {realtorID}";
                adapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                adapter.Fill(dataset);

            }
        }

        private void dealLink_Click(object sender, EventArgs e)
        {
            DealsRealtor dealsr = new DealsRealtor();
            dealsr.realtorID = realtorID;
            this.Hide();
            dealsr.Show();
        }

        private void Profile_Enter(object sender, EventArgs e)
        {

        }

        private void Realtor_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
