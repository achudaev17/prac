﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Data.SqlClient;
namespace Agency
{
    public partial class Auth : Form
    {
        Font fontBold1,fontLight1,fontMedium1;
        public Auth()
        {
            InitializeComponent();
        
            //title.Font = fontBold1;
            //login.Font = fontLight1;
            //pass.Font = fontMedium1;
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataSet dataset = new DataSet();
        bool auth = false;
        Selection selection = new Selection();
        dataBase database = new dataBase();
        Realtor realtor = new Realtor();
        Client client = new Client();

        private void label2_Click(object sender, EventArgs e)
        {
            Registration reg = new Registration();
            this.Hide();
            reg.Show();
        }

        private void Auth_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != "" && textBox2.Text != "")
            {
                using (SqlConnection connection = new SqlConnection(database.connect()))
                {
                    adapter = new SqlDataAdapter("select Логин,Пароль,Фамилия,Имя,ID_Риэлтора from Риэлторы", connection);
                    builder = new SqlCommandBuilder(adapter);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    if (textBox1.Text.Trim() != "")
                    {
                        if (textBox2.Text.Trim() != "")
                        {
                            for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                            {
                                if (textBox1.Text == dataset.Tables[0].Rows[i][0].ToString())
                                {
                                    if (textBox2.Text == dataset.Tables[0].Rows[i][1].ToString())
                                    {
                                        auth = true;
                                        realtor.realtorID = dataset.Tables[0].Rows[i][4].ToString();
                                        this.Hide();
                                        realtor.Show();
                                    }
                                }
                            }
                            adapter = new SqlDataAdapter("select Электронная_почта,Пароль,Фамилия,Имя,ID_Клиента from Клиенты", connection);
                            builder = new SqlCommandBuilder(adapter);
                            dataset = new DataSet();
                            adapter.Fill(dataset);
                            for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                            {
                                if (textBox1.Text == dataset.Tables[0].Rows[i][0].ToString())
                                {
                                    if (textBox2.Text == dataset.Tables[0].Rows[i][1].ToString())
                                    {
                                        auth = true;
                                        client.clientID = dataset.Tables[0].Rows[i][4].ToString();
                                        client.Greeting.Text += dataset.Tables[0].Rows[i][3].ToString();
                                        this.Hide();
                                        client.Show();
                                    }
                                }
                            }
                            if (auth == false)
                            {
                                MessageBox.Show("Неправильный логин или пароль!", "Ошибка входа", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Поле пароля не должно быть пустым!", "Ошибка входа", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Поле логина не должно быть пустым!", "Ошибка входа", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
            }
            else
            {
                MessageBox.Show("Заполните поля для авторизации", "Ошибка авторизации", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //private void loadFont()
        //{
        //    PrivateFontCollection fontcastom = new PrivateFontCollection();
        //    fontcastom.AddFontFile("Roboto-Bold.ttf");
        //    fontcastom.AddFontFile("Roboto-Light.ttf");
        //    fontcastom.AddFontFile("Roboto-Medium.ttf");
        //    fontBold1 = new Font(fontcastom.Families[0],16);
        //    fontLight1 = new Font(fontcastom.Families[0],14);
        //    fontMedium1 = new Font(fontcastom.Families[1], 12);
        //}
    }
}
