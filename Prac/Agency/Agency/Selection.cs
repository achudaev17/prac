﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Agency
{
    public partial class Selection : Form
    {
        public Selection()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        dataBase db = new dataBase();
        public string clientID;
        DataSet dataset = new DataSet();
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void type_SelectedIndexChanged(object sender, EventArgs e)
        {
                if (type.SelectedIndex == 1)
                {
                    dataGridView1.DataSource = db.getData("getAppartments");
                    minRooms.ForeColor = Color.Black;
                    maxRooms.ForeColor = Color.Black;
                    maxFloor.ForeColor = Color.Black;
                    minFloor.ForeColor = Color.Black;
                    maxFloors.ForeColor = Color.Black;
                    minFloors.ForeColor = Color.Black;
                    minSquare.ForeColor = Color.Black;
                    maxSquare.ForeColor = Color.Black;

                    minSquare.ForeColor = Color.Red;
                    maxSquare.ForeColor = Color.Red;
                    minPrice.ForeColor = Color.Red;
                    maxPrice.ForeColor = Color.Red;
                    minRooms.Enabled = true;
                    minRooms.ForeColor = Color.Red;
                    maxRooms.ForeColor = Color.Red;
                    maxRooms.Enabled = true;
                    minFloor.ForeColor = Color.Red;
                    maxFloor.ForeColor = Color.Red;
                    minFloor.Enabled = true;
                    maxFloor.Enabled = true;
                }
                else if (type.SelectedIndex == 2)
                {
                    dataGridView1.DataSource = db.getData("getHouses");
                    minRooms.ForeColor = Color.Black;
                    maxRooms.ForeColor = Color.Black;
                    maxFloor.ForeColor = Color.Black;
                    minFloor.ForeColor = Color.Black;
                    maxFloors.ForeColor = Color.Black;
                    minFloors.ForeColor = Color.Black;
                    minSquare.ForeColor = Color.Black;
                    maxSquare.ForeColor = Color.Black;

                    minSquare.ForeColor = Color.Red;
                    maxSquare.ForeColor = Color.Red;
                    minPrice.ForeColor = Color.Red;
                    maxPrice.ForeColor = Color.Red;
                    minRooms.ForeColor = Color.Red;
                    maxRooms.ForeColor = Color.Red;
                    minRooms.Enabled = true;
                    maxRooms.Enabled = true;
                    minFloors.ForeColor = Color.Red;
                    maxFloors.ForeColor = Color.Red;
                    minFloors.Enabled = true;
                    maxFloors.Enabled = true;
                }
                else if (type.SelectedIndex == 3)
                {
                    dataGridView1.DataSource = db.getData("getDistricts");
                    minRooms.ForeColor = Color.Black;
                    maxRooms.ForeColor = Color.Black;
                    maxFloor.ForeColor = Color.Black;
                    minFloor.ForeColor = Color.Black;
                    maxFloors.ForeColor = Color.Black;
                    minFloors.ForeColor = Color.Black;
                    minSquare.ForeColor = Color.Black;
                    maxSquare.ForeColor = Color.Black;

                    minPrice.ForeColor = Color.Red;
                    maxPrice.ForeColor = Color.Red;
                    minSquare.ForeColor = Color.Red;
                    maxSquare.ForeColor = Color.Red;
                }
        } 

        private void Selection_Load(object sender, EventArgs e)
        {
            district.SelectedIndex = 0;
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {
                string query = "select Наименование from Районы";
             
                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    district.Items.Add(dataset.Tables[0].Rows[i][0]);
                }
            }
            type.SelectedIndex = 0;
            minRooms.Enabled = false;
            maxRooms.Enabled = false;
            minFloors.Enabled = false;
            maxFloors.Enabled = false;
            minFloor.Enabled = false;
            maxFloor.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }

        private void apply_Click(object sender, EventArgs e)
        {   
            if (type.SelectedIndex != 0 && district.SelectedIndex != 0)
            {
                if (minPrice.Text == "" && maxPrice.Text == "" && maxSquare.Text == ""
                && minSquare.Text == "" && minRooms.Text == "" && maxRooms.Text == ""
                && maxFloor.Text == "" && minFloor.Text == "" && minFloors.Text == ""
                && maxFloors.Text == "")
                {
                    MessageBox.Show("Заполните необходимые формы!", "Ошибка сортировки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (type.SelectedIndex == 1)
                    {
                        if (minPrice.Text != "" && maxPrice.Text != "" && maxSquare.Text != ""
                            && minSquare.Text != "" && minRooms.Text != "" && maxRooms.Text != ""
                            && maxFloor.Text != "" && minFloor.Text != "")
                        {

                            dataGridView1.DataSource = db.getInfo("getAppartments", $" where Цена between {minPrice.Text} and {maxPrice.Text} and Площадь between {minSquare.Text} and {maxSquare.Text}" +
                                $" and Количество_комнат between {minRooms.Text} and {maxRooms.Text}" +
                                $" and Этаж between {minFloor.Text} and {maxFloor.Text}  and Район = '{district.SelectedItem.ToString()}'");
                        }
                        else
                        {
                            MessageBox.Show("Заполните необходимые формы!", "Ошибка сортировки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    if (type.SelectedIndex == 2)
                    {
                        if (minPrice.Text != "" && maxPrice.Text != "" && maxSquare.Text != ""
                            && minSquare.Text != "" && minRooms.Text != "" && maxRooms.Text != ""
                            && maxFloors.Text != "" && minFloors.Text != "")
                        {
                            dataGridView1.DataSource = db.getInfo("getHouses", $"where Цена between {minPrice.Text} and {maxPrice.Text} and Площадь between  {minSquare.Text} and {maxSquare.Text}" +
                           $" and Количество_комнат between {minRooms.Text} and {maxRooms.Text}" +
                           $" and Этаж between {minFloors.Text} and {maxFloors.Text} and  Район = '{district.SelectedItem.ToString()}'");
                        }                          
                        else
                        {
                            MessageBox.Show("Заполните необходимые формы!", "Ошибка сортировки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    if (type.SelectedIndex == 3)
                    {
                        if (minPrice.Text != "" && maxPrice.Text != "" && maxSquare.Text != "")
                        {
                            dataGridView1.DataSource = db.getInfo("getDistricts", $"where Цена between {minPrice.Text} and {maxPrice.Text} and Площадь between  {minSquare.Text} and {maxSquare.Text}  and Район = '{district.SelectedItem.ToString()}'");
                        }
                        else
                        {
                            MessageBox.Show("Заполните необходимые формы!", "Ошибка сортировки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Укажите, пожалуйста, тип недвижимости и желаемый район", "Ошибка сортировки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialog;
            dialog = MessageBox.Show("Подтвердите действие.", "Отправка заявки", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            if (dialog == DialogResult.OK)
            {
                using (SqlConnection connection = new SqlConnection(db.connect()))
                {
                    db.insertInfo("Заявки_на_покупку_недвижимости(ID_Клиента,ID_Объекта_недвижимости,Статус)", $"'{clientID}','{maskedTextBox1.Text}','1'");
                }
            }
            MessageBox.Show("Заявка успешно отправлена.", "Отправка заявки", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        }

        private void district_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (type.SelectedIndex == 1)
            {
                dataGridView1.DataSource = db.getInfo("getAppartments", $"where Район like '{district.SelectedItem.ToString()}'");
                minRooms.ForeColor = Color.Black;
                maxRooms.ForeColor = Color.Black;
                maxFloor.ForeColor = Color.Black;
                minFloor.ForeColor = Color.Black;
                maxFloors.ForeColor = Color.Black;
                minFloors.ForeColor = Color.Black;
                minSquare.ForeColor = Color.Black;
                maxSquare.ForeColor = Color.Black;

                minSquare.ForeColor = Color.Red;
                maxSquare.ForeColor = Color.Red;
                minPrice.ForeColor = Color.Red;
                maxPrice.ForeColor = Color.Red;
                minRooms.Enabled = true;
                minRooms.ForeColor = Color.Red;
                maxRooms.ForeColor = Color.Red;
                maxRooms.Enabled = true;
                minFloor.ForeColor = Color.Red;
                maxFloor.ForeColor = Color.Red;
                minFloor.Enabled = true;
                maxFloor.Enabled = true;
            }
            else if (type.SelectedIndex == 2)
            {
                dataGridView1.DataSource = db.getInfo("getHouses", $"where Район like '{district.SelectedItem.ToString()}'");
                minRooms.ForeColor = Color.Black;
                maxRooms.ForeColor = Color.Black;
                maxFloor.ForeColor = Color.Black;
                minFloor.ForeColor = Color.Black;
                maxFloors.ForeColor = Color.Black;
                minFloors.ForeColor = Color.Black;
                minSquare.ForeColor = Color.Black;
                maxSquare.ForeColor = Color.Black;

                minSquare.ForeColor = Color.Red;
                maxSquare.ForeColor = Color.Red;
                minPrice.ForeColor = Color.Red;
                maxPrice.ForeColor = Color.Red;
                minRooms.ForeColor = Color.Red;
                maxRooms.ForeColor = Color.Red;
                minRooms.Enabled = true;
                maxRooms.Enabled = true;
                minFloors.ForeColor = Color.Red;
                maxFloors.ForeColor = Color.Red;
                minFloors.Enabled = true;
                maxFloors.Enabled = true;
            }
            else if (type.SelectedIndex == 3)
            {
                dataGridView1.DataSource = db.getInfo("getDistricts",$"where Район like '{district.SelectedItem.ToString()}'");
                minRooms.ForeColor = Color.Black;
                maxRooms.ForeColor = Color.Black;
                maxFloor.ForeColor = Color.Black;
                minFloor.ForeColor = Color.Black;
                maxFloors.ForeColor = Color.Black;
                minFloors.ForeColor = Color.Black;
                minSquare.ForeColor = Color.Black;
                maxSquare.ForeColor = Color.Black;

                minPrice.ForeColor = Color.Red;
                maxPrice.ForeColor = Color.Red;
                minSquare.ForeColor = Color.Red;
                maxSquare.ForeColor = Color.Red;
            }
        }

        private void profileLink_Click(object sender, EventArgs e)
        {
            Client client = new Client();
            client.clientID = clientID;
            this.Hide();
            client.Show();
        }

        private void Selection_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }

        private void dealLink_Click(object sender, EventArgs e)
        {
            Deal deal = new Deal();
            this.Hide();
            deal.clientID = clientID;
            deal.Show();
        }
    }
}
