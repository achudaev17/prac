﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Agency
{
    public partial class Deal : Form
    {
        public Deal()
        {
            InitializeComponent();
        }

        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        dataBase db = new dataBase();
        public string clientID;
        private void Deal_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.getInfo("selectRequestsOnBuy", $"where Клиент = {clientID}");
            dataGridView2.DataSource = db.getInfo("selectRequestsOnSell", $"where Клиент = {clientID}");
        }

        private void profileLink_Click(object sender, EventArgs e)
        {
            Client client = new Client();
            client.clientID = clientID;
            this.Hide();
            client.Show();
        }

        private void offerLink_Click(object sender, EventArgs e)
        {
            Selection selection = new Selection();
            selection.clientID = clientID;
            this.Hide();
            selection.Show();
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }

        private void Deal_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
